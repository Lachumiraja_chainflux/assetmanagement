import { Component, OnInit } from '@angular/core';
declare let require:any;
 var s= require('../browser.js')
import { FormBuilder, Validators ,FormGroup,FormControl} from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import {NG_VALIDATORS} from '@angular/forms';

var client = require('ontology-dapi').client;
client.registerClient({});

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.scss']
})

export class UpdateProductComponent implements OnInit {
  public pkk_id;
  public pk_brand;
  public pk_quant;
  public pk_price;


  angForm: FormGroup;
  submitted = false;

  constructor(private router:Router,private spinner: NgxSpinnerService,private fb: FormBuilder) { }

  public id1: any;
  public id2: any;
  public s1= 'L3duZU71BfrAHwNTffWQzSPrc2n9rVXnhx3As2Q9xVLSPb11vuTc';// particular acc WIF
  public privat5 = s.Crypto.PrivateKey.deserializeWIF(this.s1)   
  public from = new s.Crypto.Address('ANSdDAN2r2HNFawckYi12xXsZY2aNkyVdH')//('AXJFA9quEdQ4uZHv47Y5nCxiJfs5hAynYx')// b9d6d8c12ac9089d01a83eb8d419cde4136edd6f
  //Receiver's address
  public to = new s.Crypto.Address('AXJFA9quEdQ4uZHv47Y5nCxiJfs5hAynYx') //AXJFA9quEdQ4uZHv47Y5nCxiJfs5hAynYx,  AMa2ytW688ZVxLdbiH4qpP5DYfd4WckTsF
  //Amount to send
  public amount = 1
  //Asset type
  public assetType = 'ONT'
  //Gas price and gas limit are to compute the gas costs of the transaction.
  public gasPrice = '500';
  public gasLimit = '20000';
  //Payer's address to pay for the transaction gas
  public payer = this.from;
  public name='sandhiya';
     

  UpdateProduct()
   {
    this.submitted = true;  
    // stop here if form is invalid
    if (this.angForm.invalid) {
      return;
    }
console.log(this.angForm.value)

    this.spinner.show();   
    const p2 = new s.Parameter('pid', s.ParameterType.BigInteger,Number(this.pkk_id)); 
    const p4 = new s.Parameter('quantity',s.ParameterType.BigInteger,Number(this.pk_quant));
    const p5 = new s.Parameter('price',s.ParameterType.BigInteger,Number(this.pk_price));
   
    const tx5 = s.TransactionBuilder.makeInvokeTransaction("update_Product",[p2,p4,p5], 
      new s.Crypto.Address(s.utils.reverseHex('8e416524240e92d02bb41bb960c27e9d704f12c9')),
       
      String(this.gasPrice),
      String(this.gasLimit)
      
      );
       
   tx5.payer.value = this.from.value;
   const st1 = s.TransactionBuilder.signTransaction(tx5,this.privat5);
   const rest7 = new s.RestClient('http://polaris1.ont.io:20334');
   rest7.sendRawTransaction(tx5.serialize()).then(res => {  

    if(res['Desc'] == "SUCCESS")
    {
      this.spinner.hide();
      this.angForm.reset();
      location.reload();
      this.Product_Length();
      
    }
    else
    {
        this.spinner.hide();
    }


    })
      
    }


  public Product_Length()
  {
    const rest9 = new s.RestClient('http://polaris1.ont.io:20334');     
    var k;

    rest9.getStorage("8e416524240e92d02bb41bb960c27e9d704f12c9",'49645f4e756d626572').then(res2 =>{    
    k = s.utils.hexstring2ab(res2.Result);

    return this.ProductDetails(k);
    });

  }

  public myarray:any=[];
   
  public async  ProductDetails(m)
  {
    this.myarray.length =0;
    var tx7;
    var i;
   
    for( i=1;i<=m[0];i++)
    {  
      try{
        var g1 = new s.Parameter('pk_id', s.ParameterType.BigInteger,(i));
        tx7 =   await client.api.smartContract.invokeRead( { scriptHash: "8e416524240e92d02bb41bb960c27e9d704f12c9",
        operation: "get_p_details",args: [g1] })
        
        tx7[3] = s.utils.hexstring2ab(tx7[3])
        tx7[4] = s.utils.hexstring2ab(tx7[4])
        tx7[0] = s.utils.hexstring2ab(tx7[0])
        tx7[1] = s.utils.hexstr2str(tx7[1])
        tx7[2] = s.utils.hexstr2str(tx7[2])
    
        tx7[3]= tx7[3][0];
        tx7[4]= tx7[4][0];
  
        this.myarray.push(tx7)
      }
      catch(e) {
     console.log(e)
      }
      finally{ }
    }
  
 
      const rest9 = new s.RestClient('http://polaris1.ont.io:20334');
      
      rest9.getStorage("8e416524240e92d02bb41bb960c27e9d704f12c9",'4349443031').then(res1 =>{
  
});

}


ngOnInit() {
  this.Product_Length();
  this.angForm = this.fb.group({
    pkk_id: ['', Validators.required],
    // pk_brand: ['', Validators.required],
    pk_quant: ['', [Validators.required]],
    pk_price: ['', [Validators.required]]
});  
 
}


get f() { return this.angForm.controls; }
  
}


